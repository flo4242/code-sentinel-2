from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from os.path import join

import pandas as pd
from scipy import stats


def make_uci_adult_dataset(data_folder, data_file, n_samples=1000):
    train_file = 'adult.data'
    test_file = 'adult.test'
    feat_names = ['age', 'education', 'ethnicity', 'gender', 'hours_per_week',
                  'income']
    use_col_ids = [0, 4, 8, 9, 12, 14]
    df_full = pd.concat([
            pd.read_csv(join(data_folder, train_file), usecols=use_col_ids,
                        names=feat_names, skipinitialspace=True),
            pd.read_csv(join(data_folder, test_file), usecols=use_col_ids,
                        skiprows=1, names=feat_names, skipinitialspace=True)
        ])

    # subsample
    df_sample = df_full.sample(n_samples)

    # binarize ethnicity attribute: White or not White
    df_sample['ethnicity'] = pd.get_dummies(df_sample['ethnicity'])['White']

    # clean the 'income' column
    df_sample['income'] = df_sample['income'].str.strip('.')

    # prepare numeric data (used for ctSNE embedding)
    df_data = df_sample.copy()

    # Transform string-valued binary attributes to 0-1 valued
    df_data['gender'] = pd.factorize(df_data['gender'])[0]
    df_data['income'] = pd.factorize(df_data['income'])[0]

    # create data attributes d_0, ..., d_n
    data_col_names = []
    for idx in range(df_sample.shape[1]):
        data_col_names.append('d_'+str(idx))
    df_data.columns = data_col_names
    df_data[:] = stats.zscore(df_data.values)

    # add label combinations
    labels, _ = pd.factorize(df_sample['ethnicity'].astype(str) +
                             df_sample['gender'].astype(str))
    df_sample.insert(6, 'ethnicity_gender', labels)
    labels, _ = pd.factorize(df_sample['ethnicity'].astype(str) +
                             df_sample['income'].astype(str))
    df_sample.insert(7, 'ethnicity_income', labels)
    labels, _ = pd.factorize(df_sample['gender'].astype(str) +
                             df_sample['income'].astype(str))
    df_sample.insert(8, 'gender_income', labels)
    labels, _ = pd.factorize(df_sample['ethnicity'].astype(str) +
                             df_sample['gender'].astype(str) +
                             df_sample['income'].astype(str))
    df_sample.insert(9, 'ethnicity_gender_income', labels)

    # concatenate features with numeric data
    df_result = pd.concat([df_sample, df_data], axis=1)
    df_result.index.name = 'id'

    # output
    df_result.to_csv(join(data_folder, data_file))
