from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os.path
import sys
from os.path import join

import numpy as np
import pandas as pd
import pylab as plt

sys.path.append('../')
import ctsne.bhtsne as bhtsne

def compute_embedding(df_data, emb_path, prior_col=None, beta=None, method='ctsne'):
    if os.path.exists(emb_path):
        df_emb = pd.read_csv(emb_path)
        return df_emb.iloc[:, df_emb.columns.get_loc('d_0'):] \
                    .values.astype(np.float32)
    start_col = df_data.columns.get_loc('d_0')
    X = df_data.iloc[:, start_col:].values.astype(np.float32)
    labels = pd.factorize(df_data[prior_col])[0]  if prior_col \
        else np.random.randint(2, size=X.shape[0])

    if beta is None:
        beta = 1 if method == 'tsne' else 0.0001
    Y = bhtsne.run_bh_tsne(X, labels, beta=beta, initial_dims=X.shape[1],
                           verbose=True, theta=0.001, max_iter=1500)

    col_names = []
    for i in range(Y.shape[1]):
        col_names.append('d_' + str(i))
    df_emb = pd.DataFrame(Y, columns=col_names)
    df_emb.index.name = 'id'
    df_emb.to_csv(emb_path)

    return Y
