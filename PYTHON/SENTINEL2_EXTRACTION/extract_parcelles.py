from datetime import date
from collections import OrderedDict

import os
from os import listdir
from os.path import isfile, join
import os.path
from os import path
import sys
import fiona #lecture de geojson
from functools import partial
import pickle #IO de variables
import time


import pandas as pd #data frames à la R
import numpy as np 

from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
import rasterio
from rasterio import plot
from rasterstats.io import Raster #surcouche de rasterio permettant d'extraire rapidement des pixels et des stats depuis un polygone et un raster
import rasterio.features
import rasterio.warp
import shapely.geometry
from shapely import speedups
speedups.disable() #enleve l'appel à des fonctions C (voir https://stackoverflow.com/questions/62075847/using-qgis-and-shaply-error-geosgeom-createlinearring-r-returned-a-null-pointer)
import shapely.wkt
import pyproj
import shapely.geometry as shpgeo
from shapely.ops import transform




#Projection de WGS84:4326 vers UTM31N:32631 (à adapter si zone UTM différente)
project = partial(
    pyproj.transform,
    pyproj.Proj(init='epsg:4326'),
    pyproj.Proj(init='epsg:32631')) 



def toFromUTM(shp, proj, inv=False):
    geoInterface = shp.__geo_interface__

    shpType = geoInterface['type']
    coords = geoInterface['coordinates']
    if shpType == 'Polygon':
        newCoord = [[proj(*point, inverse=inv) for point in linring] for linring in coords]
    elif shpType == 'MultiPolygon':
        newCoord = [[[proj(*point, inverse=inv) for point in linring] for linring in poly] for poly in coords]

    return shpgeo.shape({'type': shpType, 'coordinates': tuple(newCoord)})



downloadImages= True
DOWNLOAD_FOLDER='K:\\SENTINEL2\\'
#Variables globales liées à l'arborescence des données Sentinel téléchargées
RPATH1='\\GRANULE\\'
RPATH2='\\IMG_DATA\\R20m\\'
RPATH2b='\\QI_DATA\\'
CLOUDMASKNAME="MSK_CLDPRB_20m.jp2"
API_USER='florianrancon'
API_PW='etsaviti'
PARCELLES_VIGNE='K:\\SENTINEL2\\parcelles_limit30k_GEE_VRC_1000.geojson'
EMPRISE_ZONE='K:\\SENTINEL2\\LIMITES34_WGS84_poly.geojson'
COLLECTION_FILENAME='K:\\SENTINEL2\\collection.pkl'
NFEATURES=9



#classes à prévoir
#Collection de données, contient des parcelles
#parcelle, id, coordonnées, contient un pixel moyen (fonctions pour mettre à jour sur la base des pixels)
#valeur de pixel, tableau, coordonnées et appartenance à une parcelle
#Mesure de terrain, type de mesure



# Catalogue d'image => Mis à jour au moment du téléchargement
############################# CLASSE SINGLETON
#classe d'image (à l'instance l'image n'est pas chargée):
# - Ensemble de noms d'objets Raster
# - Nom de l'image
# - Métadonnées prise de vue
###########################



#Classe catalogue en singleton pour n'avoir qu'un seul vrai objet et retourner facilement l'instance si on veut la récupérer autre part
#Notes: exploitent les particularités des mataclasses et une syntaxe propre à python 3, point intéressant: ça marche si on rajoute de l'héritage
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls] #déjà instancié, on redonne l'instance existante
class ImageCatalogue(metaclass=Singleton):
    def __init__(self):
        self.listeImages = []
    def push_image(self,sentinelImage):
        self.listeImages.append(sentinelImage)


class SentinelImage:
    def __init__(self, metadata, bandes):
        self.metadata = metadata
        self.bandes = bandes


class GlobalCollection:
    def __init__(self):
        self.listeParcelleCollection = []
        self.nPixels=0
        self.nParcelles=0
        self.nDates=0
    def push_parcelle(self,parcelle):
        self.listeParcelleCollection.append(parcelle)
    def exportToTable(self,saveBool) :
        print("exportation sous forme de table")
        #Allocation de la structure des pixels
        #Contient self.nPixels lignes
        #Colonnes:
        # - Numéro de parcelle
        # - Numéro de dates (pour la parcelle)
        # - Coordonneés x
        # - Coordonnées y
        # - Valeurs (NFEATURES)
        #out = np.empty([self.nPixels*self.nParcelles*self.nDates, NFEATURES+4], dtype=np.uint8)
        # info = np.iinfo(data.dtype) # Get the information of the incoming image type
        # data = data.astype(np.float64) / info.max # normalize the data to 0 - 1
        # data = 255 * data # Now scale by 255
        iParcelle=0
        first=True
        for parcelle in self.listeParcelleCollection:
            print("Mise en forme parcelle: ",iParcelle)
            iParcelle=iParcelle+1
            liste_pixel=list(parcelle.pixel)
            for iDate in range(len(parcelle.dates)):
                nPixelsDate=parcelle.mat[iDate].shape[1]
                curMat=np.array(parcelle.mat[iDate])
                #MATRICES ENTIERES
                if (not np. all(curMat == 0)):
                    #print("matrice valide")
                    curMat=np.flip(curMat, 1)
                    #print(parcelle.dates[iDate])
                    #print(liste_pixel)
                    f = lambda x,index:tuple( i[index] for i in x)

                    #On ajoute ça à la structure pandas
                    data={'parcelle': np.repeat(iParcelle,nPixelsDate),
                        'date': np.repeat(parcelle.dates[iDate],nPixelsDate),
                        'x': f(liste_pixel,0),
                        'y': f(liste_pixel,1),
                        "parcelle_x": parcelle.x,
                        "parcelle_y": parcelle.y
                    }
                    i=0
                    for row in curMat:
                        #print(pd.DataFrame(row))
                        if (not np. all(row == 0)):
                            data['s'+str(i+1)]=row
                            i=i+1
                    #print(data)
                    df2 = pd.DataFrame(data, columns = data.keys())
                    if (first):
                        df=df2
                    else:                  
                        df=pd.concat([df, df2])
                        #print(df)
                else:
                    print("matrice de zeros jetée à la date: ", parcelle.dates[iDate])
                #MATRICE MOYENNE
                if (not (curMat==0).any()):
                    mean_spectrum=np.mean(curMat, axis=1)
                    #On ajoute ça à la structure pandas
                    #Note importante: on met les valeurs uniques dans une liste parce que pandas n'aime pas créer un tableau avec seulement des scalaires
                    data={'parcelle': [iParcelle],
                        'date':[parcelle.dates[iDate]],
                        'parcelle_x': parcelle.x,
                        'parcelle_y': parcelle.y
                    }
                    i=0
                    for row in mean_spectrum:
                        #print(pd.DataFrame(row))
                        data['s'+str(i+1)]=[row]
                        i=i+1
                    df2 = pd.DataFrame(data, columns = data.keys())
                    if (first):
                        df_mean=df2
                        first=False
                    else:                  
                        df_mean=pd.concat([df_mean, df2])
                        #print(df)

        df.to_csv(DOWNLOAD_FOLDER+'RPG_full.csv') 
        df_mean.to_csv(DOWNLOAD_FOLDER+'RPG_mean.csv') 
        print("enregistrement csv terminé")






class ParcelleCollection:
    def __init__(self):
        self.mat = []
        self.dates= []
        self.pixel= [] #tuple x/y
        self.x=0
        self.y=0
        #Note: on ne fait pas de préallocation numpy car non efficace (stockage mémoire contigue façon vector), on attend d'assembler une matrice avant de faire l'allocation finale
        #Le choix du type se fera aussi au moment de l'allocatoin (a priori uint8)


# with open(DOWNLOAD_FOLDER+'collection_1000parcelles.pkl', "rb") as input_file:
#     global_collection = pickle.load(input_file)[0]
#     global_collection.exportToTable(True)





#Ajouter fonction de complétion du catalogue si nécessaire
catalogue=ImageCatalogue() #instnce unique du catalogue
#TELECHARGEMENTS D'IMAGES VIA API COPERNICUS
if (downloadImages): 
    #Identification pour accéder à l'API sentinel de copernicus
    api = SentinelAPI(API_USER, API_PW, 'https://scihub.copernicus.eu/dhus')
    #Elements de recherche, zone occitanie
    #Notes: le 31 se réfère à la zone UTM au niveau de larégion
    #tiles = ['31TEJ','31TFJ','31TFK','31TDJ','31TDH']

    footprint = geojson_to_wkt(read_geojson(EMPRISE_ZONE))
    #Renseigner ici sentinel 1 et 2, on fera la fusion plus tard entre les deux
    query_kwargs = {
            'platformname': 'Sentinel-2',
            'producttype': 'S2MSI2A',
            'date': ('20190501', '20190830')}
    products = OrderedDict()
    kw = query_kwargs.copy()
    pp = api.query(footprint,**kw)
    products.update(pp) #mise à jour en ne rajoutant des acquisitions que si elles ne sont pas déjà présentes
    #Avant de faire le téléchargement on utilise products pour créer le catalogue d'images temporelles
    print(len(products))
    #On vérifie si les pdoduits ont déjà été téléchargés
    products_filtered=OrderedDict()
    for key, value in products.items():
        if (path.isdir(DOWNLOAD_FOLDER+value.get('filename'))): #On supprime l'entrée des produits
            print("Présent en local (ne sera pas téléchargé): ", key)
        else:
            print("Sera téléchargé: ", key)
            products_filtered[key]=value

    try:
        api.download_all(products_filtered, 'K:\\SENTINEL2')
    except IOError as e:
        print('I/O error({0}): {1}'.format(e.errno, e.strerror))
    except:
        print('Unexpected error:', sys.exc_info()[0])
    for key, value in products.items():
        print("Integration catalogue de ",key)
        try:
            nomImage = listdir(DOWNLOAD_FOLDER+value.get('filename')+RPATH1)[0]
            #Création de la liste des chemins entiers vers les bandes de l'image
            prePath=DOWNLOAD_FOLDER+value.get('filename')+RPATH1+nomImage+RPATH2
            listeCanaux = [prePath+f for f in listdir(prePath) if isfile(join(prePath, f)) & f.endswith('.jp2')]
            image = SentinelImage(value, listeCanaux)
            catalogue.push_image(image)
        except IOError as e:
            print('I/O error({0}): {1}'.format(e.errno, e.strerror))
        except: 
            print('Unexpected error:', sys.exc_info()[0])   

else:
    #On récupère le catalogue s'il a déjà été créé
    print("récupération du catalogue actuel")




#Import des parcelles de vignes de la région
with fiona.drivers():
    with fiona.open(PARCELLES_VIGNE) as source:
        polygons = [shapely.geometry.shape(pol['geometry']) for pol in source]
        geoms = [pol['geometry'] for pol in source]


globalCollection=GlobalCollection()

t1=time.perf_counter()
compt=0
for parcelle_polygons, parcelle_geom in zip(polygons, geoms):
    globalCollection.nParcelles=globalCollection.nParcelles+1
    compt=compt+1
    # if compt > 10:
    #     break
    print("Parcelle ", compt)
    parcelleActuelle=ParcelleCollection()
    #On boucle sur les images et on regarde si elles ont des pixels sur cette parcelle
    tempMat=[]
    overlayFlag=False
    overlayString=""
    parcelleActuelle.x=parcelle_polygons.bounds[0]
    parcelleActuelle.y=parcelle_polygons.bounds[1]
    for sentinelImage in catalogue.listeImages:
        #On doit vérifier si le polygone de la parcelle est inclut dans l'image
        #On fait une requete shapely au début pour trouver la première image de la série qui se superpose
        #On stocke son identifiant de zone
        #Les images suivantes seront sélectionnées sur la base de la présence de cet identifiant
        if (not overlayFlag):
            footprint=sentinelImage.metadata.get('footprint')
            #Le footprint est en WGS84 non projeté, on le projete en UTM zone 31N (voir project)
            footprintPolygon = shapely.wkt.loads(footprint)
            footprintPolygon_UTM = transform(project, footprintPolygon)
            overlay=shapely.geometry.MultiPolygon(footprintPolygon_UTM).contains(parcelle_polygons)
            index=sentinelImage.metadata.get('identifier').index("_T")
            overlayString=sentinelImage.metadata.get('identifier')[index:index+7]
            print("overlaystring toruvé: ", overlayString)
            overlayFlag=True
        else:
            #Regarder si la chaine de caractère est présente
            overlay=overlayString in sentinelImage.metadata.get('identifier')
        #print(footprintPolygon)
        #print(footprintPolygon_UTM) 
        #print(parcelle_polygons)
        if (overlay):
            #On doit extraire le raster de la couverture nuageuse et éventuellement filtrer les pixels s'ils tombent dedans
            cloudPath=sentinelImage.bandes[0].split(RPATH2)[0]+RPATH2b+CLOUDMASKNAME
            with Raster(cloudPath) as cloud_mask:
                globalCollection.nDates=globalCollection.nDates+1
                print("Image superposée ", sentinelImage.metadata.get('identifier'))
                #On peut extraire le polygone pour cette image
                bandMat=[]
                for band in sentinelImage.bandes:
                    #Vérifier que l'image contienne une bande (présence de "_B" dans la chaine)
                    if ("_B" in band):
                        with Raster(band) as raster_obj:
                            raster_subset = raster_obj.read(bounds=parcelle_polygons.bounds) 
                            cloud_subset = cloud_mask.read(bounds=parcelle_polygons.bounds) 
                            #Déterminer un masque binaire des nuages à l'aide du champs de bits de l'image
                            #Approximation: on considère que seul le premier bit est important pour déterminer si c'est un nuage
                            polygon_mask = rasterio.features.geometry_mask(geometries=[parcelle_geom],
                                out_shape=(raster_subset.shape[0], 
                                    raster_subset.shape[1]),
                                transform=raster_subset.affine,
                                all_touched=False,
                                invert=True)
                            #On rajoute la bande filtrée par le masque nuages (note: divisé par 10000 pour obtenir la réflectance)
                            bandMat.append([a*b for a,b in zip(raster_subset.array[polygon_mask]/10000,1-(cloud_subset.array[polygon_mask] & 1))])
                            #Positions des pixels
                            if (not parcelleActuelle.pixel):
                                #print(np.where(polygon_mask))
                                parcelleActuelle.pixel=zip(*np.where(polygon_mask))
                                globalCollection.nPixels=globalCollection.nPixels+np.sum(polygon_mask) #Incrémentation du compte pixel total
                            #Vérifier que les pixels extraits ne soient pas nuls
                            #for pixel in raster_subset.array[polygon_mask]:
            #bandMat contient alors la matrice des valeurs pour une date
            tempMat.append(bandMat) #On rajoute une date
            parcelleActuelle.dates.append(sentinelImage.metadata.get('beginposition'))
            
    parcelleActuelle.mat=np.array(tempMat)
    globalCollection.push_parcelle(parcelleActuelle)
    print("Fin parcelle")
t2=time.perf_counter()

print("temps écoulé récup pixels:",t2-t1 )
print(sys.getsizeof(globalCollection))

with open(DOWNLOAD_FOLDER+'collection_parcelles34_2018.pkl', 'wb') as f: 
    pickle.dump([globalCollection], f)


print("fini")


globalCollection.exportToTable(True)