testAleatoireSpatial <- function(df, variable, xname, yname,ntirages=1000, nbreaks=10) {
    
  #Les valeurs des breaks d?pendent des valeurs min/max
  dm <- dist(df[,variable], method = "euclidean", diag = T) 
  minBreak=min(dm)
  maxBreak=max(dm)
  a=matrix(0,nrow=nbreaks-1)
  
  for (i in 1:ntirages) {
    dfShuffle=data.frame(iy=df[,yname], ix=df[,xname], NDVI=sample(df[,variable]))
    distance.matrix1 <- dist(dfShuffle[,c(1,2)], method = "euclidean", diag = T) 
    distance.matrix2 <- dist(dfShuffle[,c(3)], method = "euclidean", diag = T) 
    a=a+hist(distance.matrix2[distance.matrix1<1.5],plot=FALSE, breaks = seq(minBreak, maxBreak, length.out = nbreaks))$counts
  }
  a=a/ntirages
  #divergence de kullback de nouveaux tirages au tirage moyen
  vecD <- vector()
  for (i in 1:ntirages) {
    dfShuffle=data.frame(iy=df[,yname], ix=df[,xname], NDVI=sample(df[,variable]))
    distance.matrix1 <- dist(dfShuffle[,c(1,2)], method = "euclidean", diag = T) 
    distance.matrix2 <- dist(dfShuffle[,c(3)], method = "euclidean", diag = T) 
    D= kl.dist(as.matrix(hist(distance.matrix2[distance.matrix1<1.5],plot=FALSE, breaks = seq(minBreak, maxBreak, length.out = nbreaks))$counts), a, base = 2)$D
    vecD=c(vecD,D)
  }
  #On peut alors regarder o? se place l'?chantillon original
  distance.matrix1 <- dist(df[,c(xname,yname)], method = "euclidean", diag = T) 
  distance.matrix2 <- dist(df[,variable], method = "euclidean", diag = T) 
  b=as.matrix(hist(distance.matrix2[distance.matrix1<1.5],plot=FALSE, breaks = seq(minBreak, maxBreak, length.out = nbreaks))$counts)
  Dsampl= kl.dist(b, a, base = 2)$D
  #Et on calcule la proba associ?e
  proba=sum(vecD>Dsampl)/length(vecD)
  #dfPixelId$randomness[subsetPixelId]=proba
  return(proba)
}